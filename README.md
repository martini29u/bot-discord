## Arche-Discord-Bot
Bot Discord pour le serveur ArcheRP.

## Description
Gestion de Tickets, Création de Sondage et Message de Bienvenue.

## Execution
A la racine du fichier, ouvrez un terminal.
Lancer le programme avec nodemon : npm run dev
Lancer le programme avec node : npm run start

## Support
Florent#4751 on Discord.

## Authors and acknowledgment
Created by Florent 'Pingouin' Martini.

## License
Copyright © Florent 'Pingouin' Martini.
MIT License.

