/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Listener } = require('discord-akairo');
const { CHANNELBIENVENUE } = require('../utils/config.json');

class GuildMemberAddListener extends Listener {
    constructor() {
        super('guildMemberAdd', {
            emitter: 'client',
            event: 'guildMemberAdd'
        });
    }

    exec(member) {
        const embed = this.client.functions.embed()
            .setColor('fe019a')
            .setTitle(this.client.variables.MessageBienvenue[0])
            .setDescription("Bienvenue sur nos rives <@"+member.id+"> !")
            .addField(this.client.variables.MessageBienvenue[1], this.client.variables.MessageBienvenue[2], false)
        return member.guild.systemChannel.send({ embeds: [ embed ] });
    }
}

module.exports = GuildMemberAddListener;