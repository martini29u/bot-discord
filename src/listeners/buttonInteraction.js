/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Listener } = require('discord-akairo');
const { MessageButton, MessageActionRow } = require('discord.js');

class ButtonInteractionListener extends Listener {
    constructor() {
        super('interactionCreate', {
            emitter: 'client',
            event: 'interactionCreate'
        });
    }

    exec(interaction) {
        interaction.deferUpdate();
        //L'interaction est un boutton
        if(interaction.isButton()) {
            //On regarde si elle fait partie des 4 bouttons de Ticket
            for(let i=0; i<4; i++) {
                if(interaction.message.id==this.client.variables.TicketID[i]) { //Si c'est le cas :
                    //On crée un channel avec les bonnes permissions
                    this.client.variables.TicketCount[i]++;
                    interaction.guild.channels.create("Ticket #"+this.client.variables.TicketCount[i], {
                        type: 'GUILD_TEXT', //Salon textuel
                        permissionOverwrites: [
                           {
                             id: interaction.guild.roles.everyone, //Pour tout le monde
                             deny: ['VIEW_CHANNEL', 'READ_MESSAGE_HISTORY', 'SEND_MESSAGES'] //Supprimer les perms
                           }
                        ],
                    }).then(channel => {
                        channel.permissionOverwrites.edit(interaction.user, {
                            VIEW_CHANNEL: true,
                            READ_MESSAGE_HISTORY: true,
                            SEND_MESSAGES: true
                        })
                        for(let j=0; j<this.client.variables.TicketRoles[i].length; j++) {
                            channel.permissionOverwrites.edit(channel.guild.roles.cache.find(role => role.id === this.client.variables.TicketRoles[i][j]), {
                                VIEW_CHANNEL: true,
                                READ_MESSAGE_HISTORY: true,
                                SEND_MESSAGES: true
                            }).catch(err => {
                                console.log(err);
                                return this.client.functions.error(interaction.message);
                            });
                        }
                        const embed = this.client.functions.embed()
                            .setDescription('Un membre du staff viendra répondre à votre demande dans les plus brefs délais. Pour fermer le ticket, réagissez avec :lock:')
                            .setColor("#007B28");
                        const button = new MessageButton()
                            .setCustomId('closeTicket')
                            .setLabel('Fermer le ticket')
                            .setStyle('SECONDARY')
                            .setEmoji('🔒');
                        const row = new MessageActionRow()
                            .addComponents(
                                button
                            );
                        channel.send({ content : "<@"+interaction.user.id+"> Bienvenue", embeds: [embed], components: [row] })
                    })
                
                }
            }
            if(interaction.customId === 'closeTicket') {
                const button1 = new MessageButton()
                    .setCustomId('closeTicketConfirm')
                    .setLabel('Fermer')
                    .setStyle('PRIMARY');
                const button2 = new MessageButton()
                    .setCustomId('closeTicketCancel')
                    .setLabel('Annuler')
                    .setStyle('SECONDARY');
                const row = new MessageActionRow()
                    .addComponents(
                        button1,
                        button2
                    );
                interaction.message.channel.send({ content : "Êtes-vous sûr de vouloir fermer le ticket ?", components: [row] })    
            }
            else if(interaction.message.channel.name.includes('ticket-')) {
                if(interaction.customId === 'closeTicketConfirm') {
                    interaction.message.channel.delete();
                }
                else {
                    interaction.message.delete();
                }
            }
        }
    }
}

module.exports = ButtonInteractionListener;