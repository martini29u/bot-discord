/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { AkairoClient, CommandHandler, ListenerHandler } = require("discord-akairo");
const { error, embed } = require('../utils/functions');
const { TOKEN } = require("../utils/config.json");

module.exports = class client extends AkairoClient {
    constructor(config = {}) {
        super(
            { ownerID: '314517738615865344' },
            { 
                allowedMentions: {
                    parse: ['roles', 'everyone', 'users'],
                },
                partials: ['CHANNEL', 'GUILD_MEMBER', 'MESSAGE', 'REACTION', 'USER'],
                presence: {
                    status:'online',
                    activities: [
                        {
                            name: '?help',
                            type: 'LISTENING',
                        }
                    ]
                },
                intents: 32767
            }
        );

        this.commandHandler = new CommandHandler(this, {
            prefix: config.prefix,
            defaultCooldown: 2000,
            directory: './src/commands/'
        });

        this.listenerHandler = new ListenerHandler(this, {
            directory: './src/listeners/'
        })

        this.functions = {
            error: error,
            embed: embed
        }

        this.variables = {
            SondageMessageID: 'None',
            TicketID: new Array(4), //MJ = 0, Support = 1, Question = 2, Admin = 3;
            TicketRoles: new Array(4),
            TicketCount: [0,0,0,0],
            MessageBienvenue: ['None', 'None', 'None']
        }
    }

    init() {
        this.commandHandler.useListenerHandler(this.listenerHandler);
        this.commandHandler.loadAll();
        this.listenerHandler.loadAll();
        console.log("Commands -> " + this.commandHandler.modules.size);
        console.log("Listeners -> " + this.listenerHandler.modules.size);
    }

    async start() {
        await this.init();
        return this.login(TOKEN);
    }
}