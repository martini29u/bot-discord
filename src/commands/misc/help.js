/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Command } = require('discord-akairo');

class HelpCommand extends Command {
    constructor() {
        super('help', {
            aliases: ['help','aides','aide']
        });
    }

    exec(message) {
        message.delete();
        //Construction de l'embed
        const embed = this.client.functions.embed()
            .setColor('RED')
            .setDescription(":interrobang: **Aides :**")
            .addFields(
                { name:"?ping", value:"Permet de savoir si je suis présent.", inline:false },
                { name:"?credits", value:"Affiche les crédits du bot.", inline:false },
                { name:"?help | ?aides | ?aide", value:"Affiche les différentes commandes possibles.", inline:false },
                { name:"?CreateSondage <TITRE>", value:"Créé un sondage avec le TITRE.", inline:false },
                { name:"?AddReactionSondage <EMOJI> <INTITULÉ>", value:"Ajoute INTITULÉ comme choix de réponse en réagissant avec EMOJI", inline:false },
                { name:"?CreateTicket <TYPE> <ROLE_ID>", value:"Créé un ticket du TYPE voulu (Admin, Support, MJ ou Question) avec les ROLE_ID qui ont le droit d'accès", inline:false },
                { name:"?BienvenueTitre <TEXTE>", value:"Ajoute TEXTE dans le message de bienvenue comme titre", inline:false },
                { name:"?BienvenueChamp1 <TEXTE>", value:"Ajoute TEXTE dans le message de bienvenue comme champ1", inline:false },
                { name:"?BienvenueChamp2 <TEXTE>", value:"Ajoute TEXTE dans le message de bienvenue comme champ2", inline:false }
            );
        return message.channel.send({ embeds: [ embed ] });
    }
}

module.exports = HelpCommand;