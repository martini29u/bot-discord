/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Command } = require('discord-akairo');

class CreditCommand extends Command {
    constructor() {
        super('credits', {
            aliases: ['credits']
        });
    }

    exec(message) {
        message.delete();
        //Construction de l'embed
        const embed = this.client.functions.embed()
            .setColor('#0571B0')
            .setTitle(":desktop: Crédits :")
            .setDescription("Bot créé par @Florent#4751 alias Pingouin");
        return message.channel.send({ embeds: [ embed ] });
    }
}

module.exports = CreditCommand;