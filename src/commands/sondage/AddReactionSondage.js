/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Command } = require('discord-akairo');

class AddReactionSondageCommand extends Command {
    constructor() {
        super('AddReactionSondage', {
            aliases: ['AddReactionSondage']
        });
    }

    exec(message) {
        let messageID;
        message.delete();
        //Au moins un emoji et un mot pour l'option
        if((message.content.split(' ')).length < 3) {
            return this.client.functions.error(message);
        }
        //On récup l'id
        messageID = this.client.variables.SondageMessageID;
        //On régarde si la commande CreateSondage a été faite avant
        if(messageID=='None') {
            return this.client.functions.error(message);
        }
        //On récupère l'émoji et le texte
        const emoji = message.content.split(' ')[1];
        let text = message.content.split(' ');
        text.shift();
        text.shift();
        //On edit l'embed + add la reaction.
        message.channel.messages.fetch({around: messageID, limit: 1}).then(msg => {
                const fetchedMsg = msg.first();
                fetchedMsg.react(emoji).catch(err => {
                    console.log(err);
                    return this.client.functions.error(message);
                });
                const embed = fetchedMsg.embeds[0].addField(emoji + ' ' + text, '__________', true);
                fetchedMsg.edit({ embeds: [ embed ] });
        });
        return;
    }
}

module.exports = AddReactionSondageCommand;