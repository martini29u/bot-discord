/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Command } = require('discord-akairo');

class CreateSondageCommand extends Command {
    constructor() {
        super('CreateSondage', {
            aliases: ['CreateSondage']
        });
    }

    exec(message) {
        message.delete();
        //Au moins un mot pour le titre
        if((message.content.split(' ')).length < 2) {
            return this.client.functions.error(message);
        }
        //On récupère le titre
        const title = message.content.substr(message.content.indexOf(" ") + 1);
        //On l'affiche dans l'embed
        const embed = this.client.functions.embed()
            .setDescription(":bar_chart: **Sondage : "+title+"**")
            .setColor('PURPLE');
        //On récup l'id pour pouvoir ajouter les réactions et on la stocke
        return message.channel.send({ embeds: [ embed ] }).then(msg => {
            this.client.variables.SondageMessageID = msg.id;
        });
    }
}

module.exports = CreateSondageCommand;