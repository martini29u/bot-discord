/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Command } = require('discord-akairo');

class BienvenueTitreCommand extends Command {
    constructor() {
        super('BienvenueTitre', {
            aliases: ['BienvenueTitre']
        });
    }

    exec(message) {
        message.delete()
        //Au moins un mot pour le titre
        if((message.content.split(' ')).length < 2) {
            return this.client.functions.error(message);
        }
        //On récupère le titre
        const title = message.content.substr(message.content.indexOf(" ") + 1);
        //On l'affiche dans l'embed
        this.client.variables.MessageBienvenue[0] = title;
    }
}

module.exports = BienvenueTitreCommand;