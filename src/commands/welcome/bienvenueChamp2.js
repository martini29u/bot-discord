/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Command } = require('discord-akairo');

class BienvenueChamp2Command extends Command {
    constructor() {
        super('BienvenueChamp2', {
            aliases: ['BienvenueChamp2']
        });
    }

    exec(message) {
        message.delete()
        //Au moins un mot pour le titre
        if((message.content.split(' ')).length < 2) {
            return this.client.functions.error(message);
        }
        //On récupère le titre
        const text = message.content.substr(message.content.indexOf(" ") + 1);
        //On l'affiche dans l'embed
        this.client.variables.MessageBienvenue[2] = text;
    }
}

module.exports = BienvenueChamp2Command;