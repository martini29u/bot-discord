/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { Command } = require('discord-akairo');
const { MessageButton, MessageActionRow } = require('discord.js')

class CreateTicketCommand extends Command {
    constructor() {
        super('CreateTicket', {
            aliases: ['CreateTicket']
        });
    }

    exec(message) {
        message.delete();
        //Deux paramètres au moins
        if((message.content.split(' ')).length < 3) {
            return this.client.functions.error(message);
        }
        //On récupère le titre
        const ticketType = message.content.split(' ')[1];
        let roles = new Array();
        roles = message.content.split(' ');
        roles.shift();
        roles.shift();
        if(ticketType.toUpperCase()!="MJ" && ticketType.toUpperCase()!="SUPPORT" && ticketType.toUpperCase()!="QUESTION" && ticketType.toUpperCase()!="ADMIN") 
        return this.client.functions.error(message);
        //On l'affiche dans l'embed
        const embed = this.client.functions.embed()
            .setTitle(":tickets: Ticket "+ticketType)
            .setDescription("Pour créer un ticket réagissez avec :ticket:")
            .setColor('#007B28');
        const button = new MessageButton()
            .setCustomId('ticket')
            .setLabel('Créer un ticket')
            .setStyle('SECONDARY')
            .setEmoji('🎫');
        const row = new MessageActionRow()
			.addComponents(
				button
			);
        //On récup l'id pour le listener et on la stocke
        return message.channel.send({ embeds: [embed], components: [row] }).then(msg => {
            switch(ticketType.toUpperCase()) {
                case "MJ":
                    this.client.variables.TicketID[0] = msg.id;
                    this.client.variables.TicketRoles[0] = roles;
                    break;
                case "SUPPORT":
                    this.client.variables.TicketID[1] = msg.id;
                    this.client.variables.TicketRoles[1] = roles;
                    break;
                case "QUESTION":
                    this.client.variables.TicketID[2] = msg.id;
                    this.client.variables.TicketRoles[2] = roles;
                    break;
                case "ADMIN":
                    this.client.variables.TicketID[3] = msg.id;
                    this.client.variables.TicketRoles[3] = roles;
                    break;
                default:
                    this.client.functions.error();
            }
        });
    }
}

module.exports = CreateTicketCommand;