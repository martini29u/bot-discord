/*******************************************/
// Copyright © Florent 'Pingouin' Martini. //
//              MIT License.               //
/*******************************************/

const { SSL_OP_EPHEMERAL_RSA } = require('constants');
const { MessageEmbed } = require('discord.js');

module.exports = {
    error: function(message) { //Commande incorrecte
        const embed = new MessageEmbed()
            .setColor('GREY')
            .setDescription(":x: Commande incorrecte. Tapez **?help** pour plus d'informations.");
        return message.channel.send({ embeds: [ embed ] });
    },
    embed: function() { //Création d'un embed
        return new MessageEmbed()
            .setFooter("Bot Discord de l'Arche", "https://cdn.discordapp.com/emojis/894022811369234445.png?size=96");
    }
}